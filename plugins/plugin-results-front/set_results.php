<?php 
/**
 * Plugin name: Résultats Compétitions TP
 * Description: Plugins et widgets TP
 * Author: <a href="#">Koko</a>
 * Version: 1.0.0
 */

 require_once plugin_dir_path(__FILE__).'/results.php';
 require_once plugin_dir_path(__FILE__).'/class_list_table_cpt.php';
 require_once plugin_dir_path(__FILE__).'/class_wp_liste_member.php';

class set_results 
{

     public function __construct() 
     {

        //On va chercher le fichier de création de la table 
        register_activation_hook(__FILE__, array ('results', 'install'));
        //Préparation de la désinstalation du plugin 
        register_uninstall_hook(__FILE__,array('results','uninstall'));
        //Préparation de la désactivation du plugin
        register_deactivation_hook(__FILE__,array('results','deactivate'));

        //ajout des widgets 
        add_action('widgets_init',function(){register_widget('class_wp_liste_member');});

        //link style et script
        add_action('wp_enqueue_scripts',array($this, 'setting_link_script'));

        // ajout du menu
        //add_action('admin_menu', array($this,'add_menu_back') );

     }

    

     /**
     * Affiche le menu en back office
     *
     * @return void
     */
    public function add_menu_back() {
        
      $hook = add_menu_page('Les Compétitions',
      'Cpt', 'manage_options',
      'Cpt', array($this,'myMenuBack'), 'dashicons-dashboard', 46);
     
      //relie l'enfant slug au parent slug pour afficher un sous-menu 
      add_submenu_page('Cpt', 'Ajouter', 'Ajouter un club',
      'manage_options', 'subclubs', array($this, 'myMenuBack'));
      
      add_action('load-$hook', array($this,'addOptions'));             
  }

  function addOptions()
    {
        $option = 'perPage';
        $args = [
            'label' => 'nom',
            'default' => 3,
            'option' => 'nom_per_page'
        ];
        add_screen_option($option, $args);
        add_filter('set-screen-option','t_set_option', 10,3);
        function t_set_option($status,$option, $value)
        {
            return $value;
        }
    }

    public function myMenuBack()
    {
        if($_GET[ 'page' ] == 'Clubs' || isset($_POST['nom']) ) { // si on est sur une page membre

            echo "<h1>".get_admin_page_title()."</h1>";
            $ins = new results();

            if(isset($_POST['nom'])){
                $ins->saveMember();
            }

            if((isset($_GET['action']) && $_GET['action'] == 'delete')){ // si on à cliquer sur un delete
                $ins->deleteById($_GET['id']);
            }

            $table = new class_list_table_cpt();
            $table->prepare_items(); 
            echo '<form id="my-filter" method="get">';
            echo '<input type="hidden" name="page" value="clubs" />';
            echo $table->search_box('search', 'searchid');
            echo $table->display();
            echo '</form>';

           /* echo '<table class="widefat fixed" cellspacing="0">';
            echo '<tr><th class="manage-column column-columnname " scope="col">Nom</th>'.
                '<th class="manage-column column-columnname " scope="col">Adresse</th>'.
               '<th class="manage-column column-columnname num " scope="col">Tel</th>'.
               '<th class="manage-column column-columnname " scope="col">Email</th></tr>';
               '<th class="manage-column column-columnname " scope="col">Participant au competitions</th></tr>';
               '<th class="manage-column column-columnname " scope="col">Domaine de prédilection</th></tr>';
            foreach ($ins->findAll() as $line){
               echo '<tr style="border:1px solid #000">';
               echo '<td>'.$line['nom'].'</td>';
                echo '<td>'.$line['adresse'].'</td>';
                echo '<td>'.$line['tel'].'</td>';
                echo '<td>'.$line['email'].'</td>';
                echo '<td>'.$line['participant_au_competition'].'</td>';
                echo '<td>'.$line['domaine_de_predilection'].'</td>';
                echo '</tr>';
           }
            echo '</table>'; */

        }  else {
            echo "<h1>".get_admin_page_title()."</h1>";

            echo '<form action="" method="post">';
            echo '<p>';
            echo '<label for="nom">Nom :</label>';
            echo '<input class="widefat" id="nom" name="nom" type="text" value="" />';
            echo '</p>';
            echo '<p>';
            echo '<label for="nom">Rue :</label>';
            echo '<input class="widefat" id="rue" name="rue" type="text" value="" />';
            echo '</p>';
            echo '<p>';
            echo '<label for="nom">Ville :</label>';
            echo '<input class="widefat" id="ville" name="ville" type="text" value="" />';
            echo '</p>';
            echo '<p>';
            echo '<label for="nom">CP :</label>';
            echo '<input class="widefat" id="cp" name="cp" type="text" value="" />';
            echo '</p>';
            echo '<p>';
            echo '<label for="nom">Tel :</label>';
            echo '<input class="widefat" id="tel" name="tel" type="text" value="" />';
            echo '</p>';
            echo '<p>';
            echo '<label for="nom">Email :</label>';
            echo '<input class="widefat" id="email" name="email" type="text" value="" />';
            echo '</p>';
            echo '<label for="nom">Domaine :</label>';
            echo '<input class="widefat" id="domaine" name="domaine" type="text" value="" />';
            echo '</p>';
            echo '<p>';
            echo '<label for="nom">Participant :</label>';
            echo '<input class="widefat" id="cpt" name="compétition" type="boolean" value="" />';
            echo '</p>';
            echo '<p>';
            echo '<p><input type="submit" value="Enregister"></p>';
            echo '</form>';
        } 


    }

    public function setting_link_script() 
        {
        wp_register_style('', plugins_url('css/.css',__FILE__));
        wp_enqueue_style('');
        }

}
new set_results();