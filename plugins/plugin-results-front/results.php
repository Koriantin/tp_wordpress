<?php

class results
{

    public function __construct() 
    {

    }

    /*public static function install() {

        global $wpdb;//connexion à la bdd
        $wpdb -> query("CREATE TABLE IF NOT EXISTS ".
        "{$wpdb->prefix}tp_clubs (id INT AUTO_INCREMENT PRIMARY KEY, ".
        "nom VARCHAR (150) NOT NULL, rue VARCHAR (150) NOT NULL, ".
        "ville VARCHAR (150) NOT NULL, cp VARCHAR (150) NOT NULL,".
        "email VARCHAR (250) NOT NULL, tel VARCHAR (100) NOT NULL, ".
        "domaine_de_predilection VARCHAR (200) NOT NULL, ".
        "participant_au_competition BOOLEAN );");
    
        $count = $wpdb->get_var("SELECT count(*) FROM".
        "{$wpdb->prefix}tp_clubs;");

        
            $wpdb->insert("{$wpdb->prefix}tp_clubs", array(
                'nom' => 'aeroargeles',
                'rue' => 'Chemin de St André Argelès Sur Mer',
                'ville' => 'argeles',
                'cp' => '66700',
                'email' => 'aeroargeles@modelisme.fr',
                'tel' => '06 23 58 30 37',
                'domaine_de_predilection' => 'aérien',
                'participant_au_competition' => true
            ));

        global $wpdb;//connexion à la bdd
        $wpdb -> query("CREATE TABLE IF NOT EXISTS ".
        "{$wpdb->prefix}tp_competitors (num_adhérent INT AUTO_INCREMENT PRIMARY KEY, ".
        "nom VARCHAR (150) NOT NULL, prenom VARCHAR (150) NOT NULL, ".
        "email VARCHAR (250) NOT NULL, tel VARCHAR (100) NOT NULL, ".
        "rue VARCHAR (200) NOT NULL, ville VARCHAR (150) NOT NULL, cp VARCHAR (150) NOT NULL, ".
        "club VARCHAR (150) NOT NULL);");

        $count = $wpdb->get_var("SELECT count(*) FROM".
        "{$wpdb->prefix}tp_competitors;");

        
            $wpdb->insert("{$wpdb->prefix}tp_competitors", array(
                'nom' => 'dnto',
                'prenom' => 'koko',
                'email' => 'koko@dnto.fr',
                'tel' => '0626307716',
                'rue' => '11 rue frederic',
                'ville' => 'PERPIGNAN',
                'cp' => '66000',
                'club' => 'aeroargeles'
            ));

        global $wpdb;//connexion à la bdd
        $wpdb -> query("CREATE TABLE IF NOT EXISTS ".
        "{$wpdb->prefix}tp_cpt_auto (num_adhérent INT AUTO_INCREMENT PRIMARY KEY, ".
        "points_courses INT, total_points INT );");

        $count = $wpdb->get_var("SELECT count(*) FROM".
        "{$wpdb->prefix}tp_cpt_auto;");

        
            $wpdb->insert("{$wpdb->prefix}tp_cpt_auto", array(
                'points_courses' => 5,
                'total_points' => 15
               
            ));
        
        global $wpdb;//connexion à la bdd
        $wpdb -> query("CREATE TABLE IF NOT EXISTS ".
        "{$wpdb->prefix}tp_cpt_drones (num_adhérent INT AUTO_INCREMENT PRIMARY KEY, ".
        "points_courses INT, total_points INT, 3_4_rotors INT NOT NULL );");

        $count = $wpdb->get_var("SELECT count(*) FROM".
        "{$wpdb->prefix}tp_cpt_drones;");

        
            $wpdb->insert("{$wpdb->prefix}tp_cpt_drones", array(
                'points_courses' => 5,
                'total_points' => 15,
                '3_4_rotors' => 3
               
            ));

        
    }*/

    //Supprime la table quand on désinstalle le plugin
    /*public static function uninstall() {

        global $wpdb;
        $wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}tp_clubs;");
        $wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}tp_competitors;");
        $wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}tp_cpt_auto;");
        $wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}tp_cpt_drones;");
    }*/

    //Vide la table membre quand on désactive le plugin
    /*public static function deactivate() {

        global $wpdb;
        $wpdb->query("TRUNCATE {$wpdb->prefix}tp_clubs;");
        $wpdb->query("TRUNCATE {$wpdb->prefix}tp_competitors;");
        $wpdb->query("TRUNCATE {$wpdb->prefix}tp_cpt_auto;");
        $wpdb->query("TRUNCATE {$wpdb->prefix}tp_cpt_drones;");
    }*/

    //Récupère tous les champs de la table membre et les stocks dans un tableau associatif
    public function findAll() {

        global $wpdb;
        $res = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}tp_clubs;", ARRAY_A);

        return $res;
    }

    public function deleteById($id)
    {
        if(!is_array($id)){
            $id = array($id);
        }
        global $wpdb;

        $wpdb->query("DELETE FROM {$wpdb->prefix}tp_clubs WHERE id in ("
            .implode(',',$id).");");
    }

    public function saveMember()
    {
        global $wpdb;
        if(isset($_POST['email']) && !empty($_POST['email'])){
            $nom = $_POST['nom'];
            $adresse = $_POST['adresse'];
            $tel = (is_numeric($_POST['tel']))?($_POST['tel']):(0);
            $email = $_POST['email'];

            $row = $wpdb->get_row("SELECT * FROM ".
                "{$wpdb->prefix}tp_clubs WHERE email='".
                $email."';");

            if(is_null($row)){ // si il n'ya pas d'enregistrement pour cet email
                $wpdb->insert("{$wpdb->prefix}tp_clubs",
                    array('nom' => $nom, 'adresse' => $adresse,
                    'tel' => $tel, 'email' => $email));
            }

        }
    }


}