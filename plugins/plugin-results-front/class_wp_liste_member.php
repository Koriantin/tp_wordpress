<?php


require_once plugin_dir_path(__FILE__) . '/results.php';

class class_wp_liste_cpt extends WP_Widget
{
    public function __construct()
    {
        $widget_ops = array(
            'classname' => 'widget_liste_membre',
            'description' => __('Affichage liste de membre'),
            'customize_selective_refresh' => true,
        );
        parent::__construct('liste_membre', __('Liste membres club', 'Liste_membre'), $widget_ops);
    }

    public function widget($args, $instance)
    {
        $ins = new results();
        echo $args['before_widget'];
        echo $args['before_title'];
        echo 'Liste des membres';
        echo $args['after_title'];
        foreach ($ins->findAll() as $line) {
            echo '<div>' . $line['nom'] . ' ' . $line['participant_au_competition'] . '</div>';
        }

        echo $args['after_widget'];
    }

    public function update($new_instance, $old_instance)
    {

    }

    public function form($instance)
    {

    }


}
