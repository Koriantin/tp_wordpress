<?php
if(! class_exists('WP_List_Table')){
    require_once (ABSPATH.'wp-admin/includes/class-wp-list-table.php');
}
require_once plugin_dir_path(__FILE__).'/results.php';

class class_list_table_cpt extends WP_List_Table
{
    private $inscription;

    public function __construct() // CONSTRUCTEUR
    {
        parent::__construct([
            'singular' => __('Member', 'sp'),
            'plural' => __('Members', 'sp'),
        ]);
        $this->inscription = new results();
    }

    /**
     * préparation de la table
     */
    public function prepare_items()
    {
        $columns = $this->get_columns();
        $hidden = $this->get_hidden_columns();
        $sortable = $this->get_sortable_columns();

        $this->process_bulk_action();

        $perPage = $this->get_items_per_page('nom_per_page', 3);
        $currentPage = $this->get_pagenum();
        $data = $this->inscription->findAll();
        $totalPage = count($data);

        usort($data, array(&$this, 'usort_reorder'));
        $paginateData = array_slice($data, (($currentPage -1) * $perPage), $perPage);

        $this->set_pagination_args([
            'total_items' => $totalPage,
            'per_page' => $perPage,
        ]);

        $this->_column_headers = array($columns, $hidden, $sortable);
        $this->items = $paginateData;

    }

    public function get_columns()
    {
        $columns = [
            'cb' => '<input type="checkbox"/>',
            'nom' => 'nom',
            'rue' => 'rue',
            'ville' => 'ville',
            'cp' => 'cp',
            'domaine_de_predilection' => 'domaine_de_predilection',
        ];
        return $columns;
    }

    public function get_hidden_columns()
    {
        return array();
    }

    public function get_sortable_columns()
    {
        return $sortable = array(
            'nom' => array('nom',false),
            'cp' => array('cp',false),
            'tel' => array('tel',false),
        );

    }

    public function column_default($item, $column_name)
    {
        switch ( $column_name ){
            case 'id':
            case 'nom':
            case 'rue':
            case 'ville':
            case 'cp':
            case 'tel':
            case 'email':
            case 'domaine_de_predilection':
            case 'participant_au_competition':
                return $item[ $column_name ];
            default:
                return print_r( $item, true );
        }
    }

    /**
     * function de trie
     * @param $a
     * @param $b
     * @return int|lt
     */
    function usort_reorder( $a, $b ) {
        // If no sort, default to title
        $orderby = ( ! empty( $_GET['orderby'] ) ) ? $_GET['orderby'] : 'nom';
        // If no order, default to asc
        $order = ( ! empty($_GET['order'] ) ) ? $_GET['order'] : 'asc';
        // Determine sort order
        $result = strcmp( $a[$orderby], $b[$orderby] );
        // Send final sort direction to usort
        return ( $order === 'asc' ) ? $result : -$result;
    }

    /**
     * ajout des selecteurs d'actions
     * @return array
     */
    function get_bulk_actions()
    {
        $actions = [
            'delete' => 'Supprimer',
        ];
        return $actions;
    }

    function column_cb($item)
    {
        return sprintf('<input type="checkbox" name="id[]" value="%s"  />',
            $item['id']);
    }

    function process_bulk_action()
    {

        if('delete' === $this->current_action()){
            $ids = isset($_REQUEST['id']) ? $_REQUEST['id'] : array();
            if(!empty($ids)){
                $this->inscription->deleteById($ids);
            }
        }
    }

    /**
     * action sur le nom
     *
     */
    function column_nom($item)
    {
        $actions = [
            'delete' => sprintf('<a href="?page=%s&action=%s&id=%s">Supprimer</a>', $_REQUEST['page'],'delete',$item['id']),
        ];
        return sprintf('%1$s %2$s', $item['nom'], $this->row_actions($actions));
    }
}
