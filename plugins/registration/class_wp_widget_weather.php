<?php

class class_wp_widget_weather extends WP_Widget
{
// définition global du widget de mon widget
    function __construct()
    {
        $widget_ops = array(
            'classname'                   => 'widget_weather',
            'description'                 => __( 'Info météo de dark sky api pour le code postal. par le plugin ERN2019' ),
            'customize_selective_refresh' => true,
        );
        parent::__construct('weather', __('Weather widget 2 le retour','Weather'), $widget_ops);
    }

    //affichage front-end
    function widget($args, $instance)
    {
        $title = 'Méteo';
        // définition de l'url d'appel de l'api
        $url = 'https://api.darksky.net/forecast/'.$instance['apiToken'].'/'.$instance['lat'].','.$instance['lon'];
        // appel de l'api
        $request = wp_remote_get($url);
        if( is_wp_error( $request ) ) { // si il y a une erreur
            return false; // Bail early
        }
        // on récupère le body de la réponse
        $body = wp_remote_retrieve_body( $request );
        // on parse le body pour transformer le json en tableau
        $data = json_decode( $body );
        echo $args['before_widget'];
        if ( $title ) {
            echo $args['before_title'] . $title . $args['after_title'];
        }
        echo '<div id="meteo_wrap" class="meteo_wrap">';
        if( ! empty( $data ) ){

            echo '<div class="'.$data->currently->icon.'">&nbsp;</div>';
            echo '<div>'.
                number_format((($data->currently->temperature -32) * (5/9)), 2,',',' ').
                ' deg</div>';
            echo '<div>'.
                number_format(($data->currently->windSpeed * 1.609), 2,',',' ').
                ' km</div>';

        }
        echo '</div>';
        echo $args['after_widget'];
    }

    // traitement des données avant sauvegarde
    function update($new_instance, $old_instance)
    {
        $instance          = $old_instance;
        $instance['lat'] = sanitize_text_field( $new_instance['lat'] );
        $instance['lon'] = sanitize_text_field( $new_instance['lon'] );
        $instance['apiToken'] = sanitize_text_field( $new_instance['apiToken'] );

        return $instance;
    }

    // Affichage du formulaire de configuration
    function form($instance)
    {
        $instance = wp_parse_args( (array) $instance, array( 'lat' => '','lon' => '',
            'apiToken' => '' ) );
        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'lat' ); ?>"><?php _e( 'Lattitude :' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'lat' ); ?>"
                   name="<?php echo $this->get_field_name( 'lat' ); ?>" type="text"
                   value="<?php echo esc_attr( $instance['lat'] ); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'lon' ); ?>"><?php _e( 'Longitude:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'lon' ); ?>"
                   name="<?php echo $this->get_field_name( 'lon' ); ?>" type="text"
                   value="<?php echo esc_attr( $instance['lon'] ); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'apiToken' ); ?>"><?php _e( 'Clé api :' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'apiToken' ); ?>"
                   name="<?php echo $this->get_field_name( 'apiToken' ); ?>" type="text"
                   value="<?php echo esc_attr( $instance['apiToken'] ); ?>" />
        </p>
        <?php
    }
}
