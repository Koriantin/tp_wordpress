<?php
/*
 * Plugin name: Inscription toysarus
 * Description: plugin et widgets variés
 * Author: Faure Sébastien
 * Version: 1.0.0
 */

require_once plugin_dir_path(__FILE__).'/class_wp_widget_weather.php';
require_once plugin_dir_path(__FILE__).'/class_wp_liste_member.php';
require_once plugin_dir_path(__FILE__).'/inscription.php';
require_once plugin_dir_path(__FILE__).'/class_list_table_member.php';

class registration
{
    public function __construct()
    {

//        new inscription();

        // on va chercher la méthode de création de la table
        register_activation_hook(__FILE__,array('inscription', 'install'));

        //register_deactivation_hook( __FILE__, array('inscription', 'deactivate') );

        // on prepare la desintallation du plugin
        register_uninstall_hook(__FILE__,array('inscription', 'uninstall'));


        add_action( 'widgets_init',
            function (){
            register_widget('class_wp_widget_weather');
            register_widget('class_wp_liste_member');
        });

        // link style et script
        add_action('wp_enqueue_scripts',
            array($this, 'setting_link_script'));

        // ajout du menu
        add_action('admin_menu', array($this,'add_menu_back') );

    }

    public function add_menu_back()
    {
        $hook = add_menu_page("Les membres Toysarus 2019",
            'Membre Toysarus', 'manage_options',
            'Toysarusmember',array($this,'myMenuBack'),
            'dashicons-admin-users',46);

        add_submenu_page('Toysarusmember', 'Ajouter un membre',
            'Ajouter', 'manage_options',
            'subernmember', array($this, 'myMenuBack'));

        add_action('load-$hook', array($this,'addOptions') );
    }

    function addOptions()
    {
        $option = 'perPage';
        $args = [
            'label' => 'nom',
            'default' => 3,
            'option' => 'nom_per_page'
        ];
        add_screen_option($option, $args);
        add_filter('set-screen-option','t_set_option', 10,3);
        function t_set_option($status,$option, $value)
        {
            return $value;
        }
    }

    public function myMenuBack()
    {

        if($_GET[ 'page' ] == 'Toysarusmember' || isset($_POST['nom']) ) { // si on est sur une page membre

            echo "<h1>".get_admin_page_title()."</h1>";
            $ins = new inscription();

            if(isset($_POST['nom'])){
                $ins->saveMember();
            }

            if((isset($_GET['action']) && $_GET['action'] == 'delete')){ // si on à cliquer sur un delete
                $ins->deleteById($_GET['id']);
            }

            $table = new class_list_table_clubs();
            $table->prepare_items();
//            echo '<form id="my-filter" method="get">';
//            echo '<input type="hidden" name="page" value="toymember" />';
//            echo $table->search_box('search', 'searchid');
            echo $table->display();
//            echo '</form>';

//            echo '<table class="widefat fixed" cellspacing="0">';
//            echo '<tr><th class="manage-column column-columnname " scope="col">Nom</th>'.
//                '<th class="manage-column column-columnname " scope="col">Prénom</th>'.
//                '<th class="manage-column column-columnname num " scope="col">Age</th>'.
//                '<th class="manage-column column-columnname " scope="col">Email</th></tr>';
//            foreach ($ins->findAll() as $line){
//                echo '<tr style="border:1px solid #000">';
//                echo '<td>'.$line['nom'].'</td>';
//                echo '<td>'.$line['prenom'].'</td>';
//                echo '<td>'.$line['age'].'</td>';
//                echo '<td>'.$line['email'].'</td>';
//                echo '</tr>';
//            }
//            echo '</table>';

        } else {
            echo "<h1>".get_admin_page_title()."</h1>";

            echo '<form action="" method="post">';
            echo '<p>';
            echo '<label for="nom">Nom :</label>';
            echo '<input class="widefat" id="nom" name="nom" type="text" value="" />';
            echo '</p>';
            echo '<p>';
            echo '<label for="nom">Prenom :</label>';
            echo '<input class="widefat" id="prenom" name="prenom" type="text" value="" />';
            echo '</p>';
            echo '<p>';
            echo '<label for="nom">Age :</label>';
            echo '<input class="widefat" id="age" name="age" type="text" value="" />';
            echo '</p>';
            echo '<p>';
            echo '<label for="nom">Email :</label>';
            echo '<input class="widefat" id="email" name="email" type="text" value="" />';
            echo '</p>';
            echo '<p><input type="submit" value="Enregister"></p>';
            echo '</form>';
        }


    }

    public function setting_link_script()
    {
        wp_register_style('meteo',
            plugins_url('css/meteo.css', __FILE__));
        wp_enqueue_style('meteo');
    }

}

new registration();
