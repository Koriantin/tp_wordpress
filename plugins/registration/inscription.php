<?php


class inscription
{


    public function __construct()
    {

    }

    public static function install()
    {
        global $wpdb;
        $wpdb->query("CREATE TABLE IF NOT EXISTS ".
            "{$wpdb->prefix}toys_member (id INT AUTO_INCREMENT PRIMARY KEY,".
            " nom VARCHAR (150) NOT NULL, prenom VARCHAR (200) NOT NULL, ".
            "age INT NOT NULL, email VARCHAR(255) NOT NULL);");

        $count = $wpdb->get_var("SELECT count(*) FROM ".
            "{$wpdb->prefix}toys_member;");

        if( $count == 0 ){
            $wpdb->insert("{$wpdb->prefix}toys_member", array(
               'nom' => 'Faure',
               'prenom' => 'Sébastien',
               'age' => 45,
               'email' => 'sebastien@lidem.eu'
            ));
        }
    }

    public static function deactivate()
    {
        global $wpdb;
        $wpdb->query("TRUNCATE {$wpdb->prefix}toys_member;");
    }

    public static function uninstall()
    {
        global $wpdb;
        // todo faire un un dump
        $wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}toys_member ;");
    }

    public function findAll()
    {
        global $wpdb;
        $res = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}toys_member;",
            ARRAY_A);
        return $res;
    }

    public function deleteById($id)
    {
        if(!is_array($id)){
            $id = array($id);
        }
        global $wpdb;

        $wpdb->query("DELETE FROM {$wpdb->prefix}toys_member WHERE id in ("
            .implode(',',$id).");");
    }

    public function saveMember()
    {
        global $wpdb;
        if(isset($_POST['email']) && !empty($_POST['email'])){
            $nom = $_POST['nom'];
            $prenom = $_POST['prenom'];
            $age = (is_numeric($_POST['age']))?($_POST['age']):(0);
            $email = $_POST['email'];

            $row = $wpdb->get_row("SELECT * FROM ".
                "{$wpdb->prefix}toys_member WHERE email='".
                $email."';");

            if(is_null($row)){ // si il n'ya pas d'enregistrement pour cet email
                $wpdb->insert("{$wpdb->prefix}toys_member",
                    array('nom' => $nom, 'prenom' => $prenom,
                    'age' => $age, 'email' => $email));
            }

        }
    }

}
