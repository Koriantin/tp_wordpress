<?php
/**
 * Plugin name: Clubs et Membres
 * Description: Plugin et widgets TP
 * Author: <a href="#">Koko</a>
 * Version: 1.0.0
 */

require_once plugin_dir_path(__FILE__) . '/clubs.php';
require_once plugin_dir_path(__FILE__) . '/members.php';
require_once plugin_dir_path(__FILE__) . '/class_list_table_clubs.php';
require_once plugin_dir_path(__FILE__) . '/class_list_table_members.php';
require_once plugin_dir_path(__FILE__) . '/class_wp_liste_member.php';
require_once plugin_dir_path(__FILE__) . '/pages/StatsRegionsPages.php';
require_once plugin_dir_path(__FILE__) . '/pages/MembersPages.php';
require_once plugin_dir_path(__FILE__) . '/pages/ClubsPages.php';

class set_clubs
{

    /**
     * @var StatsRegionsPages
     */
    private $StatsRegions;

    /**
     * @var MembersPages
     */
    private $Members;

    /**
     * @var ClubsPages
     */
    private $Clubs;

    public function __construct()
    {

        //On va chercher le fichier de création de la table
        register_activation_hook(__FILE__, array('ClubsPages', 'install'));
        register_activation_hook(__FILE__, array('MembersPages', 'install'));
        //Préparation de la désinstalation du plugin
        register_uninstall_hook(__FILE__, array('ClubsPages', 'uninstall'));
        register_uninstall_hook(__FILE__, array('MembersPages', 'uninstall'));
        //Préparation de la désactivation du plugin
        register_deactivation_hook(__FILE__, array('MembersPages', 'deactivate'));
        register_deactivation_hook(__FILE__, array('ClubsPages', 'deactivate'));

        //ajout des widgets
        add_action('widgets_init', function () {
            register_widget('class_wp_liste_member');
        });

        //link style et script
        add_action('wp_enqueue_scripts', array($this, 'setting_link_script'));

        // ajout du menu
        add_action('admin_menu', array($this, 'add_menu_back'));

        $this->StatsRegions = new StatsRegionsPages();
        $this->Members = new MembersPages();
        $this->Clubs = new ClubsPages();
    }


    /**
     * Affiche le menu en back office
     *
     * @return void
     */
    public function add_menu_back()
    {

        add_menu_page(
            'Les Clubs',
            'Clubs',
            'manage_options',
            'Clubs',
            array($this->Clubs, 'all'),
            'dashicons-groups',
            50
        );

        add_menu_page(
            'Les Membres',
            'Members',
            'manage_options',
            'Members',
            array($this->Members, 'all'),
            'dashicons-admin-users',
            46
        );

        add_menu_page(
            'Stats Regions',
            'Stats Regions',
            'manage_options',
            'StatsRegions',
            array($this->StatsRegions, 'all'),
            'dashicons-dashboard',
            55
        );

        //relie l'enfant slug au parent slug pour afficher un sous-menu
        add_submenu_page(
            'Clubs',
            'Ajouter',
            'Ajouter un club',
            'manage_options',
            'addClub', array($this->Clubs, 'addClub'));

        add_submenu_page(
            'Members',
            'Ajouter',
            'Ajouter un membre',
            'manage_options',
            'addMember', array($this->Members, 'addMember'));

        add_action('load-$hook', array($this, 'addOptions'));
    }

    function addOptions()
    {
        $option = 'perPage';
        $args = [
            'label' => 'nom',
            'default' => 3,
            'option' => 'nom_per_page'
        ];
        add_screen_option($option, $args);
        add_filter('set-screen-option', 't_set_option', 10, 3);
        function t_set_option($status, $option, $value)
        {
            return $value;
        }
    }

    public function setting_link_script()
    {
        wp_register_style('', plugins_url('css/.css', __FILE__));
        wp_enqueue_style('');
    }

}

new set_clubs();