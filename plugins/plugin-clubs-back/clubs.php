<?php
class clubs
{

    public function __construct() 
    {

    }


    public static function install() {

        global $wpdb;//connexion à la bdd

        $wpdb -> query("CREATE TABLE IF NOT EXISTS ".
        "{$wpdb->prefix}tp_clubs (id INT AUTO_INCREMENT PRIMARY KEY, ".
        "nom VARCHAR (150) NOT NULL, rue VARCHAR (150) NOT NULL, ".
        "ville VARCHAR (150) NOT NULL, cp VARCHAR (150) NOT NULL,".
        "email VARCHAR (250) NOT NULL, tel VARCHAR (100) NOT NULL, ".
        "domaine_de_predilection  ENUM('automobile', 'aerien', 'naval') NOT NULL, ".
        "participant_au_competition TINYINT(11) );");

        $count = $wpdb->get_var("SELECT count(*) FROM".
        "{$wpdb->prefix}tp_clubs;");


        self::createClubs();





        global $wpdb;//connexion à la bdd
        $wpdb -> query("CREATE TABLE IF NOT EXISTS ".
        "{$wpdb->prefix}tp_cpt_auto (num_adhérent INT AUTO_INCREMENT PRIMARY KEY, ".
        "points_courses INT, total_points INT );");

        $count = $wpdb->get_var("SELECT count(*) FROM".
        "{$wpdb->prefix}tp_cpt_auto;");


            $wpdb->insert("{$wpdb->prefix}tp_cpt_auto", array(
                'points_courses' => 5,
                'total_points' => 15

            ));

        global $wpdb;//connexion à la bdd
        $wpdb -> query("CREATE TABLE IF NOT EXISTS ".
        "{$wpdb->prefix}tp_cpt_drones (num_adhérent INT AUTO_INCREMENT PRIMARY KEY, ".
        "points_courses INT, total_points INT, 3_4_rotors INT NOT NULL );");

        $count = $wpdb->get_var("SELECT count(*) FROM".
        "{$wpdb->prefix}tp_cpt_drones;");


            $wpdb->insert("{$wpdb->prefix}tp_cpt_drones", array(
                'points_courses' => 5,
                'total_points' => 15,
                '3_4_rotors' => 3

            ));


    }


    protected static function createClubs() {
        global $wpdb;//connexion à la bdd

        $clubs = [
            [
                'nom' => 'aeroargeles',
                'rue' => 'Chemin de St André Argelès Sur Mer',
                'ville' => 'argeles',
                'cp' => '66700',
                'email' => 'aeroargeles@modelisme.fr',
                'tel' => '06 23 58 30 37',
                'domaine_de_predilection' => 'aerien',
                'participant_au_competition' => true
            ],
            [
                'nom' => 'Michel modélisme',
                'rue' => 'Chemin de michel',
                'ville' => 'perpignan',
                'cp' => '66000',
                'email' => 'autoperpi@modelisme.fr',
                'tel' => '06 13 52 25 69',
                'domaine_de_predilection' => 'automobile',
                'participant_au_competition' => false
            ],
            [
                'nom' => 'Patrick modélisme',
                'rue' => 'route de pollyestres',
                'ville' => 'pollyestres',
                'cp' => '78800',
                'email' => 'navalpollyestres@modelisme.fr',
                'tel' => '06 26 98 30 37',
                'domaine_de_predilection' => 'naval',
                'participant_au_competition' => true
            ],
        ];

        $tableName = "{$wpdb->prefix}tp_clubs";

        foreach ($clubs as $club) {
            $wpdb->insert($tableName, $club);
        }
    }

    //Supprime la table quand on désinstalle le plugin

    public static function uninstall() {

        global $wpdb;
        $wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}tp_clubs;");
        $wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}tp_cpt_auto;");
        $wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}tp_cpt_drones;");
    }
    //Vide la table membre quand on désactive le plugin

    public static function deactivate() {

        global $wpdb;
        $wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}tp_clubs;");
        $wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}tp_cpt_auto;");
        $wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}tp_cpt_drones;");
    }
    //Récupère tous les champs de la table membre et les stocks dans un tableau associatif


    public function findAll($columns = ['*']) {

        global $wpdb;
        $columns = implode($columns, ', ');
        $res = $wpdb->get_results("SELECT {$columns} FROM {$wpdb->prefix}tp_clubs;", ARRAY_A);

        return $res;
    }

    public function statsByRegion() {
        global $wpdb;

        $rqt = <<<EOF
            SELECT
                substr(clubs.cp,1,2) as region,
                max(domaine_de_predilection) as top_domaine,
                count(competitors.num_adhérent) as nb_adhérents
            FROM wp_tp_clubs AS clubs
            
            LEFT JOIN  wp_tp_competitors AS competitors
            ON clubs.id = competitors.club
            
            GROUP BY substr(clubs.cp,1,2)
            ORDER BY count(domaine_de_predilection) desc 
EOF;

        return $wpdb->get_results($rqt, ARRAY_A);
    }

    public function deleteById($id)
    {
        if(!is_array($id)){
            $id = array($id);
        }
        global $wpdb;

        $wpdb->query("DELETE FROM {$wpdb->prefix}tp_clubs WHERE id in ("
            .implode(',',$id).");");
    }

    public function saveClub()
    {
        global $wpdb;
        if(isset($_POST['email']) && !empty($_POST['email'])){
            $nom = $_POST['nom'];
            $rue = $_POST['rue'];
            $ville = $_POST['ville'];
            $cp = $_POST['cp'];
            $tel = $_POST['tel'];
            $email = $_POST['email'];
            $domaine = $_POST['domaine_de_predilection'];
            $participant = $_POST['participant_au_competition'];

            $row = $wpdb->get_row("SELECT * FROM ".
                "{$wpdb->prefix}tp_clubs WHERE email='".
                $email."';");

            if(is_null($row)){ // si il n'ya pas d'enregistrement pour cet email
                $wpdb->insert("{$wpdb->prefix}tp_clubs", [
                    'nom' => $nom,
                    'rue' => $rue,
                    'ville' => $ville,
                    'cp' => $cp,
                    'tel' => $tel,
                    'email' => $email,
                    'domaine_de_predilection' => $domaine,
                    'participant_au_competition' => intval($participant),
                ]);
            }

        }
    }


}