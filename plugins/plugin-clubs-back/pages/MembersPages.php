<?php

require_once plugin_dir_path(__FILE__) . '../clubs.php';
require_once plugin_dir_path(__FILE__) . '../members.php';

class MembersPages
{
    /**
     * @var clubs
     */
    private $Clubs;

    /**
     * @var members
     */
    private $Members;

    public function __construct()
    {
        $this->Clubs = new clubs();
        $this->Members = new members();
    }

    public function addMember()
    {
        if (isset ($_POST['nom'])) {
            $this->Members->saveMember();

            echo '<h3>Membre ajouté !</h3>';

            return;
        }

        $clubs = $this->Clubs->findAll(['id', 'nom']);

        echo "<h1>" . get_admin_page_title() . "</h1>";

        $clubsOptions = "";

        foreach ($clubs as $club) {
            $clubsOptions .= "<option value='" . $club['id'] . "'>" . $club['nom'] . "</option>";
        }

        $html = <<<EOF
                <form method="POST">
                    <p>
                        <label for="nom">Nom :</label>
                        <input class="widefat" id="nom" name="nom" type="text" value="" />
                    </p>
                    
                    <p>
                        <label for="prenom">Prenom :</label>
                        <input class="widefat" id="prenom" name="prenom" type="text" value="" />
                    </p>
                    
                    <p>
                        <label for="rue">Rue :</label>
                        <input class="widefat" id="rue" name="rue" type="text" value="" />
                    </p>
                    
                    <p>
                        <label for="ville">Ville :</label>
                        <input class="widefat" id="ville" name="ville" type="text" value="" />
                    </p>
                    <p>
                        <label for="cp">Code Postal :</label>
                        <input class="widefat" id="cp" name="cp" type="text" value="" />
                    </p>
                    <p>
                        <label for="email">Email :</label>
                        <input class="widefat" id="email" name="email" type="email" value="" />
                    </p>
                    
                    <p>
                        <label for="tel">Téléphone:</label>
                        <input class="widefat" id="tel" name="tel" type="text" value="" />
                    </p>
                    
                    <p>
                        <label for="club">Club :</label>
                        <select class="widefat" id="club" name="club">
                            {$clubsOptions}
                        </select>
                    </p>
                    
                    <p><input type="submit" value="Enregister"></p>
                </form>
EOF;
        echo $html;
    }


    public function all()
    {
        $ins = new members();

        echo "<h1>" . get_admin_page_title() . "</h1>";

        if ((isset($_GET['action']) && $_GET['action'] == 'delete')) { // si on à cliquer sur un delete
            $ins->deleteById($_GET['num_adhérent']);
        }

        $table = new class_list_table_members();
        $table->prepare_items();

        echo '<form id="my-filter" method="get">';
        echo '<input type="hidden" name="page" value="clubs" />';
        echo $table->search_box('search', 'searchid');
        echo $table->display();
        echo '</form>';
    }
}