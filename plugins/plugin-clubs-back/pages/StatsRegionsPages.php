<?php

require_once plugin_dir_path(__FILE__) . '../clubs.php';
require_once plugin_dir_path(__FILE__) . '../class_list_table_stats_region.php';

class StatsRegionsPages
{

    public function all()
    {
        echo "<h1>" . get_admin_page_title() . "</h1>";

        $table = new class_list_table_stats_region();
        $table->prepare_items();

        echo '<form id="my-filter" method="get">';
        echo '<input type="hidden" name="page" value="stats_regions" />';
        echo $table->search_box('search', 'searchid');
        echo $table->display();
        echo '</form>';
    }
}