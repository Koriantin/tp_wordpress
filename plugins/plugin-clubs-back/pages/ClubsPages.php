<?php

require_once plugin_dir_path(__FILE__) . '../clubs.php';

class ClubsPages {

    /**
     * @var clubs
     */
    private $Clubs;

    public function __construct()
    {
        $this->Clubs = new clubs();
    }

    public function addClub() {
        echo "<h1>" . get_admin_page_title() . "</h1>";

        if (isset($_POST['nom'])) {
            $this->Clubs->saveClub();

            echo '<h3>Club crée !</h3>';

            return;
        }

        $html = <<<EOF
                <form method="POST">
                    <p>
                        <label for="nom">Nom :</label>
                        <input class="widefat" id="nom" name="nom" type="text" value="" />
                    </p>
                    
                    <p>
                        <label for="rue">Rue :</label>
                        <input class="widefat" id="rue" name="rue" type="text" value="" />
                    </p>
                    
                    <p>
                        <label for="ville">Ville :</label>
                        <input class="widefat" id="ville" name="ville" type="text" value="" />
                    </p>
                    <p>
                        <label for="cp">Code Postal :</label>
                        <input class="widefat" id="cp" name="cp" type="text" value="" />
                    </p>
                    <p>
                        <label for="email">Email :</label>
                        <input class="widefat" id="email" name="email" type="email" value="" />
                    </p>
                    
                    <p>
                        <label for="tel">Téléphone:</label>
                        <input class="widefat" id="tel" name="tel" type="text" value="" />
                        <label for="domaine_de_predilection">Domaine de prédilection :</label>
                    </p>
                    
                    <p>
                        <select class="widefat" id="domaine_de_predilection" name="domaine_de_predilection">
                            <option value="automobile">Automobile</option>
                            <option value="aerien">Aerien</option>
                            <option value="naval">Naval</option>
                        </select>
                    </p>
                    <p>
                        Participe aux championnats  :
                        <label for="participant_oui">Oui</label>
                        <input class="widefat" id="participant_oui" name="participant_au_competition" type="radio" value="1" />
                        <label for="participant_non">Non</label>
                        <input class="widefat" id="participant_non" name="participant_au_competition" type="radio" value="0" />
                    </p>
                    
                    <p><input type="submit" value="Enregister"></p>
                </form>
EOF;
        echo $html;
    }
    public function all()
    {

        echo "<h1>" . get_admin_page_title() . "</h1>";


        if ((isset($_GET['action']) && $_GET['action'] == 'delete')) { // si on à cliquer sur un delete
            $this->Clubs->deleteById($_GET['id']);
        }

        $table = new class_list_table_clubs();
        $table->prepare_items();
        echo '<form id="my-filter" method="get">';
        echo '<input type="hidden" name="page" value="clubs" />';
        echo $table->search_box('search', 'searchid');
        echo $table->display();
        echo '</form>';

    }
}