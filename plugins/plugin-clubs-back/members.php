<?php
class members
{

    public function __construct() 
    {

    }


    public static function install() {

        global $wpdb;//connexion à la bdd

        self::createCompetitorsTable();
        self::createCompetitors();
    }


    protected static function createCompetitorsTable()
    {
        global $wpdb;//connexion à la bdd

        $rqt = <<<EOF
            CREATE TABLE IF NOT EXISTS {$wpdb->prefix}tp_competitors (
              num_adhérent INT AUTO_INCREMENT PRIMARY KEY,
              nom VARCHAR (150) NOT NULL,
              prenom VARCHAR (150) NOT NULL,
              email VARCHAR (250) NOT NULL,
              tel VARCHAR (100) NOT NULL,
              rue VARCHAR (200) NOT NULL,
              ville VARCHAR (150) NOT NULL,
              cp VARCHAR (150) NOT NULL,
              club INT(11) NOT NULL,
              FOREIGN KEY(club) REFERENCES {$wpdb->prefix}tp_clubs(id)
              ON DELETE CASCADE ON UPDATE CASCADE
            );
EOF;

        $wpdb -> query($rqt);
    }
    //Supprime la table quand on désinstalle le plugin

    public static function uninstall() {

        global $wpdb;
        $wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}tp_competitors;");
    }
    //Vide la table membre quand on désactive le plugin

    public static function deactivate() {

        global $wpdb;

        $wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}tp_competitors;");
    }
    //Récupère tous les champs de la table membre et les stocks dans un tableau associatif
    private static function createCompetitors()
    {
        global $wpdb;
    $competitors = [
        [
            'nom' => 'dnto',
            'prenom' => 'koko',
            'email' => 'koko@dnto.fr',
            'tel' => '0626307716',
            'rue' => '11 rue frederic',
            'ville' => 'PERPIGNAN',
            'cp' => '66000',
            'club' => 1
        ],

        [
            'nom' => 'brull',
            'prenom' => 'jordan',
            'email' => 'jordan@brull.fr',
            'tel' => '0623541586',
            'rue' => '11 rue patrick',
            'ville' => 'argeles',
            'cp' => '66700',
            'club' => 2
        ],

        [
            'nom' => 'brull',
            'prenom' => 'alex',
            'email' => 'alex@brull.fr',
            'tel' => '0635432562',
            'rue' => '11 rue merdique',
            'ville' => 'pollyestres',
            'cp' => '66000',
            'club' => 1
        ],

        [
            'nom' => 'bernard',
            'prenom' => 'jean',
            'email' => 'jean@bernard.fr',
            'tel' => '0626307725',
            'rue' => '11 rue ludique',
            'ville' => 'toulouse',
            'cp' => '78800',
            'club' => 3
        ],


    ];
        foreach ($competitors as $competitor) {
            $wpdb->insert("{$wpdb->prefix}tp_competitors",$competitor);
        }

    }

    public function findAll($columns = ['*']) {

        global $wpdb;

        foreach ($columns  as $i => $columnNom) {
            $columns[$i] = $wpdb->prefix . 'tp_competitors.' . $columnNom;
        }

        $columns = implode($columns, ', ');
        $rqt = <<<EOF
            SELECT 
            {$columns}, 
            {$wpdb->prefix}tp_clubs.nom as club_nom
            FROM {$wpdb->prefix}tp_competitors
            LEFT JOIN {$wpdb->prefix}tp_clubs
            ON club = {$wpdb->prefix}tp_clubs.id
EOF;

        $res = $wpdb->get_results($rqt, ARRAY_A);

        return $res;
    }

    public function deleteById($id)
    {
        if(!is_array($id)){
            $id = array($id);
        }
        global $wpdb;

        $wpdb->query("DELETE FROM {$wpdb->prefix}tp_competitors WHERE id in ("
            .implode(',',$id).");");
    }

    public function saveMember()
    {
        global $wpdb;
        if(isset($_POST['email']) && !empty($_POST['email'])){
            $nom = $_POST['nom'];
            $prenom = $_POST['prenom'];
            $rue = $_POST['rue'];
            $ville = $_POST['ville'];
            $cp = $_POST['cp'];
            $tel = $_POST['tel'];
            $email = $_POST['email'];
            $club = $_POST['club'];

            $row = $wpdb->get_row("SELECT * FROM ".
                "{$wpdb->prefix}tp_competitors WHERE email='".
                $email."';");

            if(is_null($row)){ // si il n'ya pas d'enregistrement pour cet email
                $wpdb->insert("{$wpdb->prefix}tp_competitors", [
                    'nom' => $nom,
                    'prenom' => $prenom,
                    'rue' => $rue,
                    'ville' => $ville,
                    'cp' => $cp,
                    'tel' => $tel,
                    'email' => $email,
                    'club' => $club,

                ]);
            }

        }
    }


}