<?php
/*** ********************************************************
 *  funcion setup
 * **********************************************************/
if ( ! function_exists( 'ern_setup' ) ) :

    function ern_setup(){
        global $content_width;
        if ( ! isset( $content_width ) ) {
            $content_width = 1250; /* pixels */
        }

        /**
        * Add default posts and comments RSS feed links to <head>.
         */
    add_theme_support( 'automatic-feed-links' );

    /**
     * Enable support for post thumbnails and featured images.
     */
    add_theme_support( 'post-thumbnails' );

    $args = array(
            'default-image'      => get_template_directory_uri() . 'img/Castle_Rock_-_009.jpg',
            'default-text-color' => '000',
            'width'              => '100%',
            'height'             => 250,
            'flex-width'         => true,
            'flex-height'        => true,
    );
    add_theme_support( 'custom-header', $args );

    }

endif;
add_action( 'after_setup_theme', 'ern_setup' );
/*** ********************************************************
 *  ajout des css
 * **********************************************************/
function ern2019_theme_name_script()
{
    wp_register_style('main_style', get_template_directory_uri().'/style.css', array(), true);
    wp_enqueue_style('main_style');
    wp_register_style('blog_style', get_template_directory_uri().'/css/blog.css', array(), true);
    wp_enqueue_style('blog_style');
    wp_register_style('bootstrap_style',
        'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css', array(),true);
    wp_enqueue_style('bootstrap_style');
}

add_action('wp_enqueue_scripts', 'ern2019_theme_name_script');

/*** ********************************************************
 *  fin ajout des css
 * **********************************************************/

/*** ********************************************************
 *  Gestion du menu principal
 * **********************************************************/
function register_menu()
{
    register_nav_menus(
        array(
            'menu-sup' => __('Menu sup')
        )
    );
}
add_action('init', 'register_menu');


class ern_walker extends Walker_Nav_Menu
{
    function start_el(&$output, $item, $depth=0, $args=array(), $id = 0)
    {

        $title = $item->title;
        $description = $item->description;
        $permalink = $item->url;

        $output .= "<li class='nav-item'>";

        //Add SPAN if no Permalink
        if( $permalink && $permalink != '#' ) {
            $output .= '<a href="' . $permalink . '" class="nav-link">';
        } else {
            $output .= '<span>';
        }

        $output .= $title;
        if( $description != '' && $depth == 0 ) {
            $output .= '<small class="description">' . $description . '</small>';
        }
        if( $permalink && $permalink != '#' ) {
            $output .= '</a>';
        } else {
            $output .= '</span>';
        }
    }

}
/*** ********************************************************
 *  fin ajout Gestion du menu principal
 * **********************************************************/

/*** ********************************************************
 *  Gestion des widgets
 * **********************************************************/

    function ern_widgets_init()
    {
        if ( function_exists('register_sidebar') ) {
            register_sidebar(array(
                'name' => __('sidebar area', 'ern2019'),
                'description' => __('La colonne a widget', 'ern2019'),
                'id' => 'sidebar-1',
                'before_widget' => '<div class="border border-info mb-3 rounded p-2">',
                'after_widget' => '</div>',
                'before_title' => '<h4 class="widget-title">',
                'after_title' => '</h4>',
            ));

            register_sidebar(array(
                'name' => __('Footer Widget 1', 'ern2019'),
                'description' => __('Les widget de pied de page', 'ern2019'),
                'id' => 'footer-1',
                'before_widget' => '<div id="%1$s" class="widget %2$s">',
                'after_widget' => '</div>',
                'before_title' => '<h5>',
                'after_title' => '</h5>',
            ));
        }
    }

add_action( 'widgets_init', 'ern_widgets_init' );

/*** ********************************************************
 *  Gestion des widgets
 * **********************************************************/
?>
