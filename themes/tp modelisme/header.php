<?php
/**
 * Template Name: header
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title><?php echo get_bloginfo('name');?></title>
    <?php wp_head(); ?> <!-- function récupérant des éléments de plugin -->
</head>
<body>
    <div class="container">
                <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top ">
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <?php
                        wp_nav_menu(array(
                            'theme_location' => 'menu-sup',
                            'container' => '',
                            'menu_class' => 'navbar-nav mr-auto',
                            'menu_id' => ' ',
                            'walker' => new ern_walker()

                        )); ?>

                        <form class="form-inline my-2 my-lg-0" action="<?php echo  home_url('/'); ?>" method="get">
                            <input name="s" class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                            <button class="btn btn-outline-info my-2 my-sm-0" type="submit">Search</button>
                        </form>
                    </div>
                </nav>
            </div>
        </div>
        <?php if ( get_header_image() ) : ?>
            <div id="site-header">
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
                    <img src="<?php header_image(); ?>" width="<?php echo absint( get_custom_header()->width ); ?>" height="<?php echo absint( get_custom_header()->height ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
                </a>
            </div>
        <?php endif; ?>
        <div class="jumbotron bg-info img-header">
            <h1 class="blog-title">

                <a href="<?php echo get_bloginfo('wpurl'); ?>">
                    <?php echo get_bloginfo('name'); ?>
                </a>
            </h1>
            <p class="lead blog-description"><?php echo get_bloginfo('description'); ?></p>
        </div>

        <div class="container">
