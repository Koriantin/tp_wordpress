<div class="recentpost_listing">
    <div class="blog-post">

        <?php if (has_post_thumbnail() ){ ?>
            <div class="post-thumb">
                <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
            </div>
        <?php } ?>

        <header class="entry-header">
            <h3><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h3>
            <?php if ( 'post' == get_post_type() ) : ?>
                <div class="blog_postmeta">
                    <div class="post-date"><?php echo get_the_date(); ?></div><!-- post-date -->

                </div><!-- .blog_postmeta -->
            <?php endif; ?>
        </header><!-- .entry-header -->

        <div class="entry-summary">
            <?php the_excerpt(); ?>
            <a href="<?php the_permalink(); ?>"><?php esc_html_e('lire plus &rarr;','ern2019'); ?></a>
        </div><!-- .entry-summary -->

    </div><!-- /.blog-post -->
</div>
