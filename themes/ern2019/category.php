<?php get_header(); ?>
    <div class="row">

        <div class="col-sm-8 blog-main">
            <header class="page-header">
                <?php
                the_archive_title( '<h1 class="entry-title">', '</h1>' );
                the_archive_description( '<div class="taxonomy-description">', '</div>' );
                ?>
            </header><!-- .page-header -->

            <?php
            if( have_posts() ) : while ( have_posts() ) : the_post();

            get_template_part('content', 'category');

            endwhile; endif;

            ?>
        </div><!-- /.blog-main -->

        <?php get_sidebar(); ?>

    </div><!-- /.row -->
<?php get_footer(); ?>
