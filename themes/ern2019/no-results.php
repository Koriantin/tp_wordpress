<?php
/**
 * The template part for displaying a message that posts cannot be found.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Kids Campus
 */
?>

		<header>
                <h1 class="entry-title"><?php esc_html_e( 'Rien trouvé', 'ern2019' ); ?></h1>
         </header>

		<?php if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

<!--		 <p>--><?php //printf( esc_html__( 'Ready to publish your first post? Get started here.', 'ern2019' ),
//                 esc_url( admin_url( 'post-new.php' ) ) ); ?><!--</p>-->

		<?php elseif ( is_search() ) : ?>

			<p><?php esc_html_e( 'Désolé nous n\'avons pas de résultat pour votre recherche, Essayé avec de nouveaux mot-clés.', 'ern2019' ); ?></p>
			<?php get_search_form(); ?>

		<?php else : ?>

			<p><?php esc_html_e( 'Il semblerait que nous ne pouvons répondre à votre recherche..', 'ern2019' ); ?></p>
			<?php get_search_form(); ?>

		<?php endif; ?>
