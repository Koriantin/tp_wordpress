<div class="col-sm-3 col-sm-offset-1 blog-sidebar">
    <div class="sidebar-module sidebar-module-inset">
        <div id="sidebar-1">
        <!--        <h4>À Propos</h4>-->
<!--        <p>Etiam porta <em>sem malesuada magna</em> mollis euismod.-->
<!--            Cras mattis consectetur purus sit amet fermentum. Aenean lacinia bibendum nulla sed consectetur.</p>-->
            <?php if ( ! dynamic_sidebar( 'sidebar-1' ) ) : ?>
                <div class="border border-info mb-3 rounded p-2">
                    <h4>A propos</h4>
                    <p><?php the_author_meta('description'); ?></p>
                </div>

                <?php if ( ! is_single() ) : ?>
                <div class="border border-info  mb-3 rounded p-2">
                    <h4>Archives</h4>
                    <ol class="list-unstyled">
                        <?php wp_get_archives('type=monthly'); ?>
                    </ol>
                </div>
                <?php  else :

                    $current = get_the_ID();
                    ?>
                <div class="border border-info mb-3 rounded p-2">
                    <h4>Autres articles</h4>
                    <ol class="list-unstyled">
                        <?php
                            $auteur_posts = new WP_Query( array ( 'author' => get_the_author()->id ) );
                            while ($auteur_posts->have_posts()): $auteur_posts->the_post();
                                    if(get_the_ID() !== $current) :
                                    ?>
                                    <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?> </a></li>
                                    <?php
                                    endif;
                            endwhile;
                        ?>
                    </ol>
                </div>
                <?php  endif;?>

            <?php else : ?>

                    <?php dynamic_sidebar( 'sidebar-1' ); ?>

            <?php endif; ?>

        </div>
    </div>
</div><!-- /.blog-sidebar -->
