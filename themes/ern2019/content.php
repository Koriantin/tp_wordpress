<div class="blog-post">
<!--    <h2 class="blog-post-title">Un article de blog</h2>-->
<!--    <p class="blog-post-meta">20 Janvier 2017 par <a href="#">Alexandre Le Grand</a></p>-->
<!--    <p>Cum sociis natoque penatibus et magnis <a href="#">dis parturient montes</a>, nascetur ridiculus mus.-->
<!--        Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum.-->
<!--        Sed posuere consectetur est at lobortis. Cras mattis consectetur purus sit amet fermentum.</p>-->
<!--    <div>-->
<!--        <h2>stock</h2>-->
<!--        --><?php //get_post_meta(the_ID(), 'STOCK',true) ;?>
<!--    </div>-->
    <h2 class="blog-post-title text-primary"><?php the_title(); ?></h2>
    <p class="blog-post-meta">
        <?php the_date(); ?> par <a href="#"><?php the_author(); ?></a>
    </p>
    <?php the_content(); ?>



</div><!-- /.blog-post -->
