<html lang="<?php language_attributes(); ?>">
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <title><?php bloginfo('name'); ?></title>
    <?php  wp_head();?>
</head>
<body>
<div class="container">
    <div class="row" id="logo">
        <img src="<?php header_image(); ?>"
             height="<?php echo get_custom_header()->height; ?>"
             width="<?php echo get_custom_header()->width; ?>" alt="" />
    </div>
    <!-- menu nav bar -->
    <nav class="navbar navbar-expand-lg navbar-light bg-primary">
        <!-- menu hamburger -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
    <?php


        wp_nav_menu(array(
        'theme_location' => 'menu-sup',
        'container'       => 'div',
        'container_class' => 'collapse navbar-collapse',
        'container_id'    => 'navbarSupportedContent',
        'menu_class' => 'navbar-nav mr-auto',
        'menu_id' => ' ',
        'walker' => new toy_walker(),
        ));
        ?>
    </nav>

