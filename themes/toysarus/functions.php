<?php

function toy_setup()
{
    global $content_width;
    if ( ! isset( $content_width ) ) {
        $content_width = 1250; /* pixels */
    }

    /**
     * Add default posts and comments RSS feed links to <head>.
     */
//    add_theme_support( 'automatic-feed-links' );

    /**
     * Enable support for post thumbnails and featured images.
     */
//    add_theme_support( 'post-thumbnails' );

    $args = array(
        'default-image'      => get_template_directory_uri() . '/images/Logo.jpg',
        'default-text-color' => '000',
        'width'              => '196',
        'height'             => '51',
        'flex-width'         => true,
        'flex-height'        => true,
    );
    add_theme_support( 'custom-header', $args );
}
add_action( 'after_setup_theme', 'toy_setup' );

function toys_theme_name_script()
{
    wp_register_style('main_style',
        get_template_directory_uri().'/style.css', array(), true);
    wp_enqueue_style('main_style');
    wp_register_style('blog_style',
        get_template_directory_uri().'/css/blog.css', array(), true);
    wp_enqueue_style('blog_style');
    wp_register_style('bootstrap_style',
        'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css', array(),true);
    wp_enqueue_style('bootstrap_style');
    wp_register_script('mon_js',get_template_directory_uri().'/js/monscript.js');
    wp_enqueue_script('mon_js');
}

add_action('wp_enqueue_scripts', 'toys_theme_name_script');

function register_menu()
{
    register_nav_menus(
        array(
            'menu-sup' => __('Menu sup'),
            'menu-soc' => __('Menu social')
        )
    );
}
add_action('init', 'register_menu');



class toy_walker extends Walker_Nav_Menu
{
    public function start_lvl(&$output, $depth = 0, $args = array())
    {
            $output .= '<ul class="dropdown-menu">';
    }

    function start_el(&$output, $item, $depth=0, $args=array(), $id = 0)
    {

        $title = $item->title;
        $description = $item->description;
        $permalink = $item->url;

        if($permalink != '#'){
            if($depth == 1){
               $output .= '';
            } else {

                $output .= "<li class='nav-item'>";
            }
        } else {
            $output .= "<li class='nav-item dropdown'>";
        }

        //Add SPAN if no Permalink
        if( $permalink && $permalink != '#' ) {
            if($depth == 1){
                $output .= '<a href="' . $permalink . '" class="dropdown-item">';
            } else {
                $output .= '<a href="' . $permalink . '" class="nav-link">';
            }
        } else {
                $output .= '<a href="' . $permalink .
                    '" class="nav-link dropdown-toggle" data-toggle="dropdown">';
        }

        $output .= $title;
        if( $description != '' && $depth == 0 ) {
            $output .= '<small class="description">' . $description . '</small>';
        }
        $output .= '</a>';
    }

    public function end_el(&$output, $item, $depth = 0, $args = array())
    {
        if($depth == 1){
            $output .='';
        }
    }

}


function toy_widgets_init()
{
    if ( function_exists('register_sidebar') ) {

        register_sidebar(array(
            'name' => __('Footer Widget 1', 'toys'),
            'description' => __('Les widgets de pied de page', 'toys'),
            'id' => 'footer-1',
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h5>',
            'after_title' => '</h5>',
        ));

        /* register_widget('wp_widget_meteoToy'); */
    }
}

add_action( 'widgets_init', 'toy_widgets_init' );

class wp_widget_meteoToy extends WP_Widget{

    public function __construct()
    {
        $widget_opts = [
            'classname' => 'widget_meteoToy',
            'description' => __('Le fameux widget météo !!'),
            'customize_selective_refresh' => true,
        ];

        parent::__construct('meteoToy',__('Meteo Widget','MeteoToy'),$widget_opts);
    }

    public function form($instance)
    {

        $instance = wp_parse_args( (array)$instance,
            array('lat'=>'', 'lon'=>'', 'apiToken'=>'') );
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('lat') ?>">Lattitude</label>
            <input class="widefat" id="<?php echo $this->get_field_id('lat') ?>"
              type="text" name="<?php echo $this->get_field_name('lat') ?>"
                value="<?php echo esc_attr($instance['lat']) ?>">
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('lon') ?>">Longitude</label>
            <input class="widefat" id="<?php echo $this->get_field_id('lon') ?>"
              type="text" name="<?php echo $this->get_field_name('lon') ?>"
                value="<?php echo esc_attr($instance['lon']) ?>">
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('apiToken') ?>">Api Dark Sky</label>
            <input class="widefat" id="<?php echo $this->get_field_id('apiToken') ?>"
              type="text" name="<?php echo $this->get_field_name('apiToken') ?>"
                value="<?php echo esc_attr($instance['apiToken']) ?>">
        </p>
        <?php
    }

    public function update($new_instance, $old_instance)
    {

        $intance = $old_instance;
        $intance['lat'] = sanitize_text_field($new_instance['lat']);
        $intance['lon'] = sanitize_text_field($new_instance['lon']);
        $intance['apiToken'] = sanitize_text_field($new_instance['apiToken']);

        return $intance;
    }

    public function widget($args, $instance)
    {
        $title = 'Méteo';
        // définition de l'url d'appel de l'api
        $url = 'https://api.darksky.net/forecast/'.$instance['apiToken']
            .'/'.$instance['lat'].','.$instance['lon'];
        // appel de l'api
        $request = wp_remote_get($url);
        if( is_wp_error( $request ) ) { // si il y a une erreur
            return false; // Bail early
        }
        // on récupère le body de la réponse
        $body = wp_remote_retrieve_body( $request );
        // on parse le body pour transformer le json en tableau
        $data = json_decode( $body );
        echo $args['before_widget'];
        if ( $title ) {
            echo $args['before_title'] . $title . $args['after_title'];
        }
        echo '<div id="meteo_wrap" class="meteo_wrap">';
        if( ! empty( $data ) ){

            echo '<div class="'.$data->currently->icon.'">&nbsp;</div>';
            echo '<div>'.
                number_format((($data->currently->temperature -32) * (5/9)),
                    2,',',' ').
                ' deg</div>';
            echo '<div>'.
                number_format(($data->currently->windSpeed * 1.609),
                    2,',',' ').
                ' km</div>';

        }
        echo '</div>';
        echo $args['after_widget'];

        return '';
    }
}

function myShortCode() {
    
    $a = shortcode_atts(['percent'=>10], $atts);
    return '<strong>Promotion de {$a["percent"]} sur cet article</strong>';
}
add_shortcode('myShort', 'myShortCode');

